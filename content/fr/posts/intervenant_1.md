---
date: 2023-12-09T10:58:08-04:00
description: "The Grand Hall"
featured_image: "/images/test.jpg"
title: "AUGIER Christophe"
---

Initialement ingénieur logiciel senior avec une expérience en systèmes embarqués, allant du matériel et du logiciel de bas niveau au logiciel de haut niveau, j'aide maintenant les clients à créer des objets connectés, de l'idée à un produit prêt pour la production.

Je suis un passionné de technologie, un fan de DIY, toujours à la recherche de projets excitants et innovants. Contactez-moi.

Spécialités : chef technique, systèmes embarqués, IoT, Linux, virtualisation, simulation, temps réel, planification de tâches.

Voici son [Linkedin](https://www.linkedin.com/in/christophe-augier/) pour plus d'information.
